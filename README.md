# react-pokedex
First generation pokedex written in react, utilizing PokeapiV2.

![React Pokedex](pokedexscreenshot.png "React Pokedex")

## Installation
- Clone repo
- Navigate to directory
- `npm install`

## Usage
- Spin up local server by running `npm start` or `yarn start`
- Navigate to localhost:3000

## Purpose
I created the React Pokedex for a fun intro to the framework. I learned a great deal about structure of React apps, and how to pass information from parent and child components. A few things that could be improved added or improved upon: make the app more responsive (looks terible on mobile), display more info about each Pokemon, add animations and more impressive css...

Overall it was a fun project, and I look forward to getting more into React!
