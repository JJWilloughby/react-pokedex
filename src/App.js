import React, { Component } from 'react';
import './App.css';

import ListItem from './components/list-item';

import pokemonData from './pokedex.js';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      allPokemon: pokemonData.Pokedex,
      currentPokemon: JSON.parse(pokemonData.Pokedex[0])
    };

    this.handleClick = this.handleClick.bind(this);
  }

  render() {
    return (
      <div className="row app">
        <div className="col-md-10 view center verticle-align">
          <div className="center">
            <img className="title" src="/assets/icons/title.png" alt=""/>
          </div>
          <div className="screen center anchor">
            <img className="anchor-top-left" src="/assets/icons/hat.png" alt=""/>
            <img className="anchor-top-right icon-md" src="/assets/icons/greatball.png" alt=""/>
            <h2><strong>{this.state.currentPokemon.name.toUpperCase()}</strong></h2>
            <img src={`assets/images/${this.state.currentPokemon.name}.jpg`} alt=""/>
            <div className="description center verticle-align">
              <h4>{this.state.currentPokemon.flavor_text_entries[1].flavor_text}</h4>
            </div>
          </div>
        </div>
        <div className="col-md-2 list-scroll">
          {this.buildPokemonTiles()}
        </div>
      </div>
    );
  }

  buildPokemonTiles() {
    return this.state.allPokemon.map((currentPokemon) => {
      return <ListItem key={JSON.parse(currentPokemon).id} pokemon={JSON.parse(currentPokemon)} clickHandler={this.handleClick}/>
    });
  }

  handleClick(newPokemon) {
    this.setState({
      allPokemon: this.state.allPokemon,
      currentPokemon: newPokemon
    });
  }
}

export default App;
