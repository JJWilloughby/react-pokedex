import React, { Component } from 'react';

export default class ListItem extends Component {
  constructor(props) {
    super(props);

    this.state = {};

    this.handleClick = this.handleClick.bind(this);
  }

  render() {
    return (
      <div className="poke-list-item" onClick={this.handleClick}>
        <div className="float-left">
          <img className="icon-sm" src="/assets/icons/pokeball.PNG" alt=""/>
        </div>
        <div className="float-right">
          #{this.props.pokemon.id} {this.props.pokemon.name.toUpperCase()}
        </div>
      </div>
    );
  }

  handleClick() {
    this.props.clickHandler(this.props.pokemon);
  }
};