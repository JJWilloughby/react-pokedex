const fs = require('fs');
const request = require('request');

let obj = {
  pokedex: []
};

const getPokemonData = (currentEntry, callback) => {
  request('http://pokeapi.co/api/v2/pokemon-species/' + currentEntry, (err, res, body) => {
    if (err) return console.log(err);
    obj.pokedex.push(body);
    if (currentEntry < 152) {
      console.log(currentEntry);
      getPokemonData(currentEntry + 1, callback);
    } else {
      callback();
    }
  });
};

getPokemonData(1, () => {
  let objStr = JSON.stringify(obj);
  fs.writeFile('../src/pokedex.json', objStr, 'utf8', () => {
    console.log('Done writing file');
  });
});